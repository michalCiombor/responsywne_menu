//------------------------------------------ baner-main START

window.addEventListener("scroll", function () {
    // opacity BUTTON SCROLL TO TOP
    (window.scrollY > 300) ? (document.querySelector(".backToTop").style.opacity = 1) : (document.querySelector(".backToTop").style.opacity = 0);


    let windowHeight = window.scrollY;
    const mainOffset = document.querySelector("main").offsetTop;
    const mainHeight = document.querySelector("main").clientHeight;
    const mainHeader = document.querySelector("header");
    // console.log(windowHeight);

    if (windowHeight <= mainHeight) {
        mainHeader.style.filter = "grayScale(" + windowHeight * 1.5 / (mainHeight) + ")";
    }
})
window.addEventListener("DOMContentLoaded", function () {

    let activeElement = 0;
    const names = ["Michał Ciombor", "Joanna Burek", "Antek Pedantek"];
    const proffesions = ["junior web-developer", "ux/ui dizajner", "łokocz"];
    const color = ["src/s2.png", "src/s1.png", "src/s3.png"];
    const mono = ["src/s2a.png", "src/s1a.png", "src/s3a.png"];

    function cahngeElements() {
        activeElement++;
        if (activeElement == color.length) {
            activeElement = 0;
        } {
            document.querySelector(".signature h1").textContent = names[activeElement];
            document.querySelector(".signature h2").textContent = proffesions[activeElement];
            document.querySelector("img.color").src = color[activeElement];
            document.querySelector("img.mono").src = mono[activeElement];
        }

    }

    setInterval(cahngeElements, 4000);

    document.querySelector(".buttonNav").addEventListener("click", function () {
        document.querySelector(".mainTitle").classList.toggle("active");
        document.querySelector("#mobile").classList.toggle("active");
        document.querySelector(".mobile").classList.toggle("active");
        document.querySelector(".buttonNav").classList.toggle("active");
        document.querySelectorAll("span.button").forEach(el => el.classList.toggle("active"));
    });
    window.addEventListener("resize", function () {
        document.querySelector(".mobile").classList.remove("active");
        document.querySelector("#mobile").classList.remove("active");
        document.querySelectorAll("span.button").forEach(el => el.classList.remove("active"));

    });

});
// ------------------------------baner-main END------------------------------


// jQuery scrollToSection----------------------------START-------------
$(".arrows").on("click", function () {
    const scrollToSection = "#main";
    $("body, html").animate({
        scrollTop: $(scrollToSection).offset().top,
    }, 1000)
});

document.querySelector(".backToTop").addEventListener("click", function () {
    window.scrollTo(0, 0);
})
// ------------------------------END------------------------------------